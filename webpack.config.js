const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

module.exports = (env, argv) => ({
  devServer: {
    allowedHosts: ['mmsmsy-local-dev.com'],
    compress: true,
    contentBase: path.resolve(__dirname, 'build'),
    disableHostCheck: true,
    https: true,
    host: process.env.NODE_ENV === 'test' ? '0.0.0.0' : 'mmsmsy-local-dev.com',
    hot: true,
    port: 3000
  },
  entry: path.resolve(__dirname, 'src/index.tsx'),
  module: {
    rules: [
      {test: /\.pug$/, loader: 'pug-loader'},
      {
        test: /\.(png|jpg|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {loader: "ts-loader"}
        ]
      }
    ]
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    }
  },
  output: {
    filename: '[name].[hash].js',
    path: path.resolve(__dirname, 'build')
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'whatever title',
      template: 'public/index.pug'
    }),
    new webpack.HotModuleReplacementPlugin({})
  ],
  resolve: {
    alias: {
      AppSource: path.resolve(__dirname, 'src/')
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  }
})