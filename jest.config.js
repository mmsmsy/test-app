module.exports = {
  moduleNameMapper: {
    "^AppSource(.*)$": "<rootDir>/src$1",
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js"
  },
  roots: ["<rootDir>/src"],
  setupFilesAfterEnv: ["<rootDir>setupTests.js"],
	testMatch: ["**/?(*.)+(spec|test).(ts|tsx)"],
	testPathIgnorePatterns: ['/node_modules/', 'build'],
	transform: {
		"^.+\\.(ts|tsx)$": "ts-jest"
	}
}