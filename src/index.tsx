import React from 'react'
import ReactDOM from 'react-dom'

import {App} from './App'

ReactDOM.render(<App/>, document.querySelector('#root'))

if (module.hot) {
  module.hot.accept(() => {console.log('hot running, hot')}), function() {
    console.log('Accepting the updated printMe module!')
  }
}