import puppeteer from 'puppeteer'

export const config: puppeteer.LaunchOptions = {
  args: [
    '--start-fullscreen'
  ],
  defaultViewport: {
    height: 768,
    width: 1360
  },
  executablePath: process.env.CHROME_PATH,
  headless: false,
  ignoreHTTPSErrors: true,
  slowMo: 25
}