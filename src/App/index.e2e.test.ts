import puppeteer, {Page, Browser} from 'puppeteer-core'

import {config} from 'AppSource/puppeteer/config'

const timeout = 10000

describe('Google', () => {
  let browser: Browser
  let page: Page

  beforeAll(async () => {
    browser = await puppeteer.launch(config)
    page = await browser.newPage()
  }, timeout)

  afterAll(async () => {
    await browser.close()
  })

  it('should be titled "Google"', async () => {
    await page.goto('https://google.com')
    await expect(page.title()).resolves.toMatch('Google')
  }, timeout)

  it('should show pug image', async () => {
    await page.goto('https://mmsmsy-local-dev.com/')
    await expect(page.title()).resolves.toMatch('whatever title')
  }, timeout)

  it('should show 500 comments fetched from JSON placeholder API', async () => {
    await page.waitForSelector("[data-test-id='commentHolder'")
    const comments = await page.$$("[data-test-id='commentHolder'")

    await expect(comments.length).toEqual(500)
  }, timeout)
})