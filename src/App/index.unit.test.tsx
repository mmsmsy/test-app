import {shallow} from 'enzyme'

import React from 'react'

import {App} from './'
import { CommentsComp } from './Comments'

describe('CommentsComp', () => {
  it('displays the amount of comments received', async () => {
    const wrapper = <App/>
    expect(shallow(wrapper).find(CommentsComp)).toHaveLength(1)
  }, 5000)
})