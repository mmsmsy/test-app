import React from 'react'
import styled from 'styled-components'

const CommentsAmount = styled.span`
  color: gray;
`
CommentsAmount.displayName = 'CommentsAmount'

const Title = styled.h1`
  font-size: 20px;
  font-weight: 700;
  line-height: 32px;
`

const CommentMetaTitle = styled.span`
  margin-right: 24px;

  color: gray;
  font-size: 14px;
  font-weight: 700;
  line-height: 24px;
`

const CommentAuthor = styled.span`
  font-size: 14px;
  font-weight: 700;
  line-height: 24px;
`

const CommentMeta = styled.div`
  display: flex;
`

const CommentBody = styled.span`
  font-size: 12px;
  line-height: 20px;
`

const CommentHolder = styled.div`
  display: flex;
  flex-direction: column;

  border: 1px solid black;
  margin-bottom: 10px;
  padding: 4px 10px;
`

const CommentsContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export interface Comment {
  postId: number,
  id: number,
  name: string,
  email: string,
  body: string
}

interface Props {
  comments: Comment[]
}

export function CommentsComp({comments}: Props) {
  return (
    <>
      <Title>Comments <CommentsAmount>({comments.length})</CommentsAmount></Title>
      <CommentsContainer>
        {comments.map(comment => (
          <CommentHolder key={comment.id} data-test-id='commentHolder'>
            <CommentMeta>
              <CommentMetaTitle>Author</CommentMetaTitle>
              <CommentAuthor>{comment.name}</CommentAuthor>
            </CommentMeta>
            <CommentBody>{comment.body}</CommentBody>
          </CommentHolder>
        ))}
      </CommentsContainer>
    </>
  )
}