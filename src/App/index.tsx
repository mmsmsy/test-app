import axios from 'axios'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

import PugPng from 'AppSource/App/pug.png'
import PugSvg from 'AppSource/App/pug.svg'
import {Comment, CommentsComp} from './Comments'

interface LogoProps {
  backgroundImage: any
}

const Logo = styled.div`
  height: 200px;
  width: 200px;

  background-image: ${(props: LogoProps) => `url(${props.backgroundImage})`};
  background-size: auto 100%;
`

const LogosContainer = styled.div`
  display: flex;
`

const Holder = styled.div`
  display: flex;
  flex-direction: column;
  
  min-height: 100vh;
  width: 100vw;
`

export function App() {
  const [comments, setComments] = useState<Comment[]>([])

  useEffect(() => {
    axios({
      method: 'GET',
      url: 'https://jsonplaceholder.typicode.com/posts/1/comments'
    })
      .then(res => {
        setComments(res.data)
      })
  }, [])

  return (
    <>
      <Holder>
        <LogosContainer>
          <Logo backgroundImage={PugPng}/>
          <Logo backgroundImage={PugSvg}/>
        </LogosContainer>
        <CommentsComp comments={comments}/>
      </Holder>
    </>
  )
}