FROM node:12

WORKDIR /usr/src/app

COPY ./ ./
COPY __mocks__ ./__mocks__
COPY public ./public

RUN npm ci

COPY . .

CMD npm start

EXPOSE 3000